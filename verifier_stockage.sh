#!/bin/bash

#verifier_stockage(){
    array=$(sed -n "/Ingrédient/,/Etapes/p" $1 | head --lines -2 | tail --lines +2)
    for this in $array; do
        cle=`echo "$this" | cut --delimiter=":" -f1` 
        #echo "cle : $cle"
        demande=`echo "$this" | cut --delimiter=":" -f2`
        let "demande=demande*$2"
        onstock=`grep "^$cle:" ingredient.txt | cut --delimiter=":" -f2`
        #echo "Demande : $demande"
        if [ -z "$onstock" ];
        then
            echo "Ingrédient $cle non existant"
            return 0
        elif [ "$demande" -gt "$onstock" ];
        then
            echo "Ingrédient $cle pas assez. Demande : $demande, stock : $onstock"
            return 0
        #else 
            #echo "Onstock : $onstock"
        fi
    done

    return 1
#}

#verifier_stockage ./liste-recettes/brownie-vegan.txt 1
#verifier_stockage ./liste-recettes/gateau-au-yaourt.txt 10