#!/bin/bash
#modification des recettes

read -p "Quelle recette voulez-vous modifier ? " re
echo "La recette à modifier est: "
head -n1 ./liste-recettes/$re
cat ./liste-recettes/$re
read -p "Que voulez-vous modifier ? (n pour nom, i pour ingrédients, e pour étapes) " mo
if [ $mo == "n" ]
then
	echo "$mo"
	read -p "Veuillez rentrer les modifications: " no
	echo "$no" > ./liste-recettes/temporaire.txt
	echo "`tail --lines=+2 ./liste-recettes/$re`" >> ./liste-recettes/temporaire.txt
	echo "Votre nom de recette $re a été modifié en temporaire.txt."
elif [ $mo == "i" ]
then
	echo "Voici la liste des ingrédients: "
	echo "`tail --lines=+5 ./liste-recettes/$re`"
	read -p "Que voulez-vous faire : modifier (modif), supprimer (supp) ou rajouter (rajout) ? : " ing1
	head -n2 ./liste-recettes/$re > ./liste-recettes/temporaire.txt
	echo "" >> temporaire.txt
	if [ $ing1 == "supp" ]
	then
		read -p "Veuillez rentrer les modifications: " ing
		for line in `grep "^-" ./liste-recettes/$re` 
		do
	                ingredient=`echo "$line" | cut --delimiter=":" -f1`
			if [ "$ing" != `echo "$ingredient" | tr -d "-"` ]
			then
		        echo "$line" >> ./liste-recettes/temporaire.txt
 		       	qte=`echo "$line" | cut --delimiter=":" -f2`
		        echo ""
			fi
		done
		tail --lines=+`grep -n "Etapes" ./liste-recettes/$re | cut --delimiter=":" -f1` ./liste-recettes/$re >> ./liste-recettes/temporaire.txt
	elif [ "$ing1" == "rajout" ]
	then
		read -p "Veuillez rentrer l'ingrédient à rajouter: " ing2
                read -p "Veuillez rentrer la quantité: " ing3
                for line in `grep "^-" ./liste-recettes/$re` 
                do
                        echo "$line" >> ./liste-recettes/temporaire.txt
                        echo ""
                done
		echo "-$ing2:$ing3" >> ./liste-recettes/temporaire.txt
		tail --lines=+`grep -n "Etapes" ./liste-recettes/$re | cut --delimiter=":" -f1` ./liste-recettes/$re >> ./liste-recettes/temporaire.txt
	elif [ "$ing1" == "modif" ] 
	then
		read -p "Veuillez rentrer l'ingrédient à modifier: " ing4
                read -p "Veuillez rentrer la quantité à modifier: " ing5
		for line in `grep "^-" ./liste-recettes/$re` 
                do
                        ingredient=`echo "$line" | cut --delimiter=":" -f1`
                        if [ "$ing4" != `echo "$ingredient" | tr -d "-"` ]
                        then
                        echo "$line" >> ./liste-recettes/temporaire.txt
                        qte=`echo "$line" | cut --delimiter=":" -f2`
                        echo ""
			else
				echo "-$ing4:$ing5" >> ./liste-recettes/temporaire.txt
                        fi
                done
		 tail --lines=+`grep -n "Etapes" ./liste-recettes/$re | cut --delimiter=":" -f1` ./liste-recettes/$re >> ./liste-recettes/temporaire.txt
	fi
else
	if [ $mo == "e" ]
	then
		head -n`grep -n "Etapes" ./liste-recettes/$re | cut --delimiter=":" -f1` ./liste-recettes/$re > ./liste-recettes/temporaire.txt
		tail --lines=+`grep -n "Etapes" ./liste-recettes/$re | cut --delimiter=":" -f1` ./liste-recettes/$re > tmp.txt
		echo "Veuillez rédiger les étapes: "
		nano tmp.txt
 		cat tmp.txt >> ./liste-recettes/temporaire.txt
		rm tmp.txt
	fi
fi

exit
