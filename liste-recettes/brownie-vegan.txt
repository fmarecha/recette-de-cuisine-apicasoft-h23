Brownie vegan
25 minutes

Ingrédients:
-farine:250
-sucre:270
-cacao_en_poudre:160
-levure:1
-sel:1
-sucre_vanillé:1
-eau:40
-huile_végétale:40

Etapes
1. Préchauffez votre four à 175°C (th.5/6).
2. Verser la farine, le sucre, le cacao, la levure et le sel. Bien mélanger
3. Verser l’eau et l’huile.
4. Bien mélanger jusqu’à obtention d’une pâte homogène.
5. Verser dans un moule légèrement huilé.
6. Cuire environ 25min, jusqu’à ce que le dessus ne brille plus.
7. Laisser refroidir une dizaine de minutes avant de découper.
