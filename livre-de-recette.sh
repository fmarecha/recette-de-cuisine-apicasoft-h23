#!/bin/bash

choix=-1
while [ $choix -ne 1 ] && [ $choix -ne 2 ] && [ $choix -ne 3 ] && [ $choix -ne 4 ] && [ $choix -ne 5 ] && [ $choix -ne 6 ] && [ $choix -ne 7 ] 
do
    clear
    echo "Bienvenue dans votre livre de recette"
    echo "Que voulez faire :"
    echo "1 : Ajouter une recette"
    echo "2 : Supprimer une recette"
    echo "3 : Modifier une recette"
    echo "4 : Choisir une recette"
    echo "5 : Proposer des recettes à partir d'ingrédients"
    echo "6 : Retirer les recettes de la poubelle"
    echo "7 : Lire une recette a voix haute"
    read -p "Rentrez votre choix : " choix
done

case $choix in
    1)
        echo ""
        echo "Vous avez choisi : Ajout d'une recette"
        echo ""
        ./ajout_recette.sh
        ;;
    2)
        echo ""
        echo "Vous avez choisi : Supprimer une recette"
        echo ""
        ./supprimer_recette.sh
        ;;
    3)
        echo ""
        echo "Vous avez choisi : Modifier une recette"
        echo ""
        ./modif.sh
        ;;
    4)
        echo ""
        echo "Vous avez choisi : Choisir une recette"
        echo ""
        ./choisi_recette.sh
        ;;
    5)
        echo ""
        echo "Vous avez choisi : Proposer des recettes à partir d'ingrédients"
        echo ""
        ./proposer_recette.sh
        ;;
    6)
        echo ""
        echo "Vous avez choisi : retirer les recettes de la poubelle"
        echo ""
        ./retrouve_poubelle.sh
        ;;
    7)
        echo ""
        echo "Vous avez choisi : Lire une recette a voix haute"
        echo ""
        ./lire_une_recette.sh
        ;;

    *)
        echo ""
        echo "Erreur de choix"
        echo ""
esac

