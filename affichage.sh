#!/bin/bash
#Affichage des recettes
#recette : nom de la recette
#nbre : nombre de personne

affichage()
{
echo 'Votre recette'
head -n2 $1
for line in `grep "^-" ./$1` 
do
 ingredient=`echo "$line" | cut --delimiter=":" -f1`
 echo "Ingrédient: $ingredient"
 qte=`echo "$line" | cut --delimiter=":" -f2`
 qte_a=$(($qte*$2))
 echo "Quantité pour votre nombre: $qte_a"
 echo "" 
done
 echo "Votre recette complète est: "
 tail --lines=+`grep -n "Etapes" ./$1| cut --delimiter=":" -f1` ./$1 
}

affichage $1 $2
