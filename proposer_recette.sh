#!/bin/bash

proposer_recette()
{
    #permet de proposer des recettes en demandant des ingrédients:
    liste_recettes_gardees=()


    # On demande à l'utilisateur les ingrédients qu'il possède
    ingredients_renseignes=()
    quantites=()
    continuer="oui"
    read -p "Pour combien de personnes : " nombre_de_personnes
    echo ""
    echo "Rentrez les ingrédients que vous possédez :"
    while [ "$continuer" != "non" ] 
    do
        read -p "Ingrédient : " ingredient
        read -p "Quantité : " quantite
        ingredients_renseignes+=($ingredient)
        quantites+=($quantite)
        read -p "Continuer la saisie ? (rentrez 'non' pour arrêter) : " continuer
    done
    

    # On itére parmi toutes les recettes de la liste
    for recette in ./liste-recettes/*; do
        liste_ingredients_recette=()
        recette=`basename $recette`
        liste_ingredients_recette+=(`grep "^-" "liste-recettes/$recette" | cut --delimiter=":" -f1 | tr -d "-"`)
        
        # On compare les ingrédients de la recette et ceux de l'utilisateur
        flag=1
        for ingredient in "${ingredients_renseignes[@]}" # itére les ingrédients fournis
        do
            if [[ ! " ${liste_ingredients_recette[*]} " =~ " ${ingredient} " ]]; then
                flag=0
            fi
        done
        if [ $flag -eq 1 ]; then
            liste_recettes_gardees+=($recette)
        fi
    done

    # On affiche les recettes gardees
    echo ""
    echo " ///////////////  RECETTE QUE VOUS POUVEZ FAIRE //////////////"
    for elem in "${liste_recettes_gardees[@]}"
    do
        echo ""
        ./affichage.sh "liste-recettes/$elem" $nombre_de_personnes
        echo ""
    done
}


proposer_recette