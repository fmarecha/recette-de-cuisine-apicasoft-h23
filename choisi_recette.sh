#!/bin/bash


verifier_stockage(){
    array=$(sed -n "/Ingrédient/,/Etapes/p" $1 | head --lines -2 | tail --lines +2)
    for this in $array; do
        cle=`echo "$this" | cut --delimiter=":" -f1` 
        #echo "cle : $cle"
        demande=`echo "$this" | cut --delimiter=":" -f2`
        let "demande=demande*$2"
        onstock=`grep "^$cle:" ingredient.txt | cut --delimiter=":" -f2`
        #echo "Demande : $demande"
        if [ -z "$onstock" ];
        then
            echo "Ingrédient $cle non existant"
            return 0
        elif [ "$demande" -gt "$onstock" ];
        then
            echo "Ingrédient $cle pas assez. Demande : $demande, stock : $onstock"
            return 0
        #else 
            #echo "Onstock : $onstock"
        fi
    done

    return 1
}

count=$(ls -1q ./liste-recettes/| wc -l)
./afficher_recettes.sh
choix=0
#echo $(seq 1 $count)
	while [ $choix -lt 1 ] || [ $choix -gt $count ]; do
		read -p "Quelle recette voulez-vous choisir ?" choix;
	done
	array=($(ls ./liste-recettes/))
	nom=`sed -n '1p;2q' ./liste-recettes/*${array[$((choix-1))]}`
	read -p "La recette que vous allez choisir est $nom, tapez 1 pour confirmer" confirm
	if [ $confirm -eq 1 ]
	then
		#echo "Yes"
		read -p "Vous avez combien des personnes" nombre
		./affichage.sh liste-recettes/${array[$((choix-1))]} $nombre
		echo ""
		verifier_stockage liste-recettes/${array[$((choix-1))]} $nombre
	fi

