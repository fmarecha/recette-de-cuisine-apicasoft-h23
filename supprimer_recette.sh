#!/bin/bash

supprimer_recette(){
	count=$(ls -1q ./liste-recettes/| wc -l)
	./afficher_recettes.sh
	choix=0
	#echo $(seq 1 $count)
	while [ $choix -lt 1 ] || [ $choix -gt $count ]; do
		read -p "Quelle recette voulez-vous supprimer ?" choix;
	done
	let "choix--"
	array=($(ls ./liste-recettes/))
	#echo `sed -n '1p;2q' ./liste-recettes/${array[$choix]}`
	read -p "La recette que vous allez supprimer est `sed -n '1p;2q' ./liste-recettes/${array[$choix]}`, tapez 1 pour confirmer" confirm
	if [ $confirm -eq 1 ]
	then
		#rm ./liste-recettes/${array[$choix]}
		mv ./liste-recettes/${array[$choix]} ./liste-recettes/.poubelle
	fi
}


supprimer_recette
